FROM node:17-alpine as build

COPY ./yarn.lock ./package.json /app/
RUN yarn --cwd /app

COPY . /app
RUN yarn --cwd /app build

ENTRYPOINT ["yarn", "--cwd=/app", "start"]

FROM nginx:1.21-alpine

COPY --from=build /app/build /usr/share/nginx/html
