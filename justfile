build:
  docker build . -t registry.gitlab.com/linney/homepage:deploy

push: build
  docker push registry.gitlab.com/linney/homepage:deploy
