import { useEffect, useState } from "react";

type HabitCellProps = {
    streak: number;
};

function HabitCell(props: HabitCellProps) {
    const streak = Math.round(props.streak);
    const green = Math.min(100 * streak, 700);

    return <div className={`h-7 bg-green-${green}`}></div>;
}

type HabitProps = {
    name: string;
};

function Habit(props: HabitProps) {
    const { name } = props;
    return (
        <div className="grid grid-cols-8">
            <span>{name}</span>
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
            <HabitCell streak={Math.random() * 7} />
        </div>
    );
}

export default function Habits() {
    return (
        <div className="bg-gray-800 overflow-hidden shadow rounded-lg col-span-2">
            <div className="px-4 py-5 border-b border-gray-900 sm:px-6">
                <h1 className="text-lg leading-6 font-medium text-gray-100">
                    Habits
                </h1>
            </div>
            <div className="px-4 py-5 sm:p-6 text-gray-200">
                <Habit name="Brush teeth" />
                <Habit name="Drink smoothie" />
                <Habit name="Exercise" />
                <Habit name="Meditate" />
            </div>
        </div>
    );
}
