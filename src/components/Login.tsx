import { Fragment, useState } from "react";
import { Dialog, Transition } from "@headlessui/react";

type LoginProps = {
    serviceName: string;
    keyStorageFunc: (apiKey: string) => void;
};

export default function Login(props: LoginProps) {
    const { serviceName, keyStorageFunc } = props;

    const [open, setOpen] = useState<boolean>(true);

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog
                as="div"
                className="fixed z-10 inset-0 overflow-y-auto"
                onClose={setOpen}
            >
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span
                        className="hidden sm:inline-block sm:align-middle sm:h-screen"
                        aria-hidden="true"
                    >
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <div className="inline-block align-bottom bg-gray-800 rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6">
                            <h1 className="text-lg text-gray-200 pb-6">
                                Authorisation for {serviceName}
                            </h1>
                            <form
                                className="space-y-6"
                                action="#"
                                method="POST"
                            >
                                <div>
                                    <label
                                        htmlFor="apikey"
                                        className="block text-sm font-medium text-gray-400"
                                    >
                                        API Key
                                    </label>
                                    <div className="mt-1">
                                        <input
                                            id="apikey"
                                            name="apikey"
                                            type="apikey"
                                            autoComplete="current-apikey"
                                            required
                                            className="appearance-none bg-gray-900 text-gray-200 block w-full px-3 py-2 border border-gray-700 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                                        />
                                    </div>
                                </div>

                                <div>
                                    <button
                                        type="submit"
                                        className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                    >
                                        Sign in
                                    </button>
                                </div>
                            </form>
                            {/* <h1 className="text-lg mb-4"> */}
                            {/*     Login to {serviceName} */}
                            {/* </h1> */}
                            {/* <div className="mb-4"> */}
                            {/*     <label */}
                            {/*         htmlFor="username" */}
                            {/*         className="block text-sm font-medium text-gray-700" */}
                            {/*     > */}
                            {/*         Username */}
                            {/*     </label> */}
                            {/*     <div className="mt-1"> */}
                            {/*         <input */}
                            {/*             type="text" */}
                            {/*             name="username" */}
                            {/*             id="username" */}
                            {/*             className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" */}
                            {/*             placeholder="john.smith" */}
                            {/*         /> */}
                            {/*     </div> */}
                            {/* </div> */}

                            {/* <div> */}
                            {/*     <label */}
                            {/*         htmlFor="apikey" */}
                            {/*         className="block text-sm font-medium text-gray-700" */}
                            {/*     > */}
                            {/*         apikey */}
                            {/*     </label> */}
                            {/*     <div className="mt-1"> */}
                            {/*         <input */}
                            {/*             type="apikey" */}
                            {/*             name="apikey" */}
                            {/*             id="apikey" */}
                            {/*             className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md" */}
                            {/*             placeholder="" */}
                            {/*         /> */}
                            {/*     </div> */}
                            {/* </div> */}
                            {/* <div className="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6"> */}
                            {/*     <div className="sm:col-span-4"> */}
                            {/*         <label */}
                            {/*             htmlFor="username" */}
                            {/*             className="block text-sm font-medium text-gray-700" */}
                            {/*         > */}
                            {/*             Username */}
                            {/*         </label> */}
                            {/*         <div className="mt-1 flex rounded-md shadow-sm"> */}
                            {/*             <input */}
                            {/*                 type="text" */}
                            {/*                 name="username" */}
                            {/*                 id="username" */}
                            {/*                 autoComplete="username" */}
                            {/*                 placeholder="john." */}
                            {/*                 className="flex-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full min-w-0 rounded-md sm:text-sm border-gray-300" */}
                            {/*             /> */}
                            {/*         </div> */}
                            {/*     </div> */}
                            {/* </div> */}
                        </div>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root>
    );
}
