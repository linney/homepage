import { Task, TodoistApi } from "@doist/todoist-api-typescript";
import { useEffect, useState } from "react";
import { CheckCircleIcon } from "@heroicons/react/outline";

const API_TOKEN = "000";

function getToday(): number {
    const date = new Date();

    return new Date(
        `${date.getUTCFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
    ).getTime();
}

function DisplayTask(props: { task: Task }) {
    const { task } = props;
    const [project, setProject] = useState<string>("");

    useEffect(() => {
        const API = new TodoistApi(API_TOKEN);
        API.getProject(task.projectId)
            .then((project) => setProject(project.name))
            .catch((e) => console.error(e));
    }, [task.projectId]);

    return (
        <div className="flex justify-between">
            <a href={task.url} className="flex items-center">
                <CheckCircleIcon className="h-5 pr-2" />
                {task.content}
                {task.due !== undefined ? (
                    Date.parse(task.due.date) < getToday() ? (
                        <span className="pl-2 text-red-600">(Overdue)</span>
                    ) : null
                ) : null}
            </a>
            <span>{project}</span>
        </div>
    );
}

export default function TodoistTasks() {
    const [tasks, setTasks] = useState<Task[]>([]);

    useEffect(() => {
        const API = new TodoistApi(API_TOKEN);
        API.getTasks({ filter: "today | overdue" })
            .then((_tasks) => setTasks(_tasks))
            .catch((e) => console.error(e));
    }, []);

    return (
        <div className="bg-white overflow-hidden shadow rounded-lg">
            <div className="bg-white px-4 py-5 border-b border-gray-200 sm:px-6">
                <h1 className="text-lg leading-6 font-medium text-gray-900">
                    Todays Tasks
                </h1>
            </div>
            <div className="px-4 py-5 sm:p-6">
                <ul>
                    {tasks.map((task) => (
                        <li key={task.id} className="py-2 sm:px-0">
                            <DisplayTask task={task} />
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
}
