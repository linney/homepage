import React from "react";
import {
    FaGitlab,
    FaEnvelope,
    FaCalendarDay,
    FaGithub,
    FaFilm,
    FaDownload,
} from "react-icons/fa";
import { IconType } from "react-icons/lib";
import Habits from "./components/Habits";
import Login from "./components/Login";
import TodoistTasks from "./components/TodoistTasks";

type Link = {
    name: string;
    url: string;
    icon: IconType;
};

function Links(props: { links: Link[] }) {
    const { links } = props;

    return (
        <ul className="divide-y divide-gray-900">
            {links.map((link) => (
                <li
                    key={link.url}
                    className="px-4 py-2 prose flex items-center"
                >
                    <link.icon className="pr-4 text-4xl h-8 text-gray-300" />
                    <a href={link.url} className="text-gray-300">
                        {link.name}
                    </a>
                </li>
            ))}
        </ul>
    );
}

function App() {
    return (
        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-gray-900 pt-8 grid grid-cols-2 gap-4">
            <div className="bg-gray-800 overflow-hidden shadow rounded-lg">
                <div className="bg-gray-800 px-4 py-5 border-b border-gray-900 sm:px-6">
                    <h1 className="text-lg leading-6 font-medium text-gray-100">
                        Main Links
                    </h1>
                </div>
                <div className="px-4 py-5 sm:p-6">
                    <Links
                        links={[
                            {
                                name: "Proton Mail",
                                url: "https://mail.protonmail.com",
                                icon: FaEnvelope,
                            },
                            {
                                name: "Proton Calendar",
                                url: "https://calendar.protonmail.com",
                                icon: FaCalendarDay,
                            },
                            {
                                name: "GitLab",
                                url: "https://gitlab.com",
                                icon: FaGitlab,
                            },
                            {
                                name: "GitHub",
                                url: "https://github.com",
                                icon: FaGithub,
                            },
                        ]}
                    />
                </div>
            </div>
            <div className="bg-gray-800 overflow-hidden shadow rounded-lg">
                <div className="bg-gray-800 px-4 py-5 border-b border-gray-900 sm:px-6">
                    <h1 className="text-lg leading-6 font-medium text-gray-100">
                        Home Network
                    </h1>
                </div>
                <div className="px-4 py-5 sm:p-6">
                    <Links
                        links={[
                            {
                                name: "Emby",
                                url: "https://emby.linney.xyz",
                                icon: FaFilm,
                            },
                            {
                                name: "qBittorrent",
                                url: "http://192.168.1.152:8080/",
                                icon: FaDownload,
                            },
                            {
                                name: "Sonarr",
                                url: "https://sonarr.linney.xyz",
                                icon: FaFilm,
                            },
                            {
                                name: "Radarr",
                                url: "https://radarr.linney.xyz",
                                icon: FaFilm,
                            },
                        ]}
                    />
                </div>
            </div>
            <Habits />
            <Login
                serviceName="HabitAPI"
                keyStorageFunc={(apiKey: string) => console.log(apiKey)}
            />
        </div>
    );
}

export default App;
